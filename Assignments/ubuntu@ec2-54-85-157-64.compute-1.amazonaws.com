#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"

#define DEAD 0
#define ALIVE 1
#define SPAWN 3
#define UPPER_THRESHOLD 3
#define LOWER_THRESHOLD 2

void printState(int, const int, int*);
void rowCpy(int*,int*, const int);
void playGame(const int, const int *, int, int, char **);
void getRow(const int, int, int*, int*);
void init(int*, int*, int, char**);
void circNeighboring(int, int, int*, int*);
void cellLogic(int *, int *, int *, int *, const int);

int main(int argc, char ** argv) {

    const int dimension = 16;
    const int global_grid[ 256 ] = {   0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                  };



    int generations = 64;

    playGame(dimension, global_grid, generations, argc, argv);
    return 0;

}

void init(int * numProcess, int * p_id, int argc, char ** argv) {
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, numProcess);
    MPI_Comm_rank(MPI_COMM_WORLD, p_id);
}

void circNeighboring(int numProcess, int p_id, int * next, int * prev) {

    if (numProcess == 1) {
        *next = 0;
        *prev = 0;
    } else {
        *next = (p_id + 1) % numProcess;
        *prev = (p_id == 0) ? numProcess - 1 : p_id - 1;
    }
}
void playGame(const int dimension, const int * global_grid, int generations, int argc, char ** argv) {
    int offset, numberElements;
    int lowerSend[dimension], upperSend[dimension];
    int lowerReceive[dimension], upperReceive[dimension];
    
    int lowRow[dimension], upRow[dimension], currRow[dimension], tmpRow[dimension];
    int * grid;
    int *tempGrid;

    int numProcess, p_id;
    int prev, next;
    MPI_Status status;

    init(&numProcess, &p_id, argc, argv);
    circNeighboring(numProcess, p_id, &next, &prev);

    if (dimension % numProcess != 0) {
        exit(0);
    }
    numberElements = (dimension * dimension) / numProcess;
    offset = p_id * numberElements;
    
    grid = malloc(sizeof(int) * numberElements);
    tempGrid = malloc(sizeof(int) * numberElements);

    int i;
    for (i = 0; i < numberElements; i++) {
        grid[i] = global_grid[i + offset];
    }
    int * receivingBuffer;
    if (p_id == 0) {
        receivingBuffer = malloc(sizeof(int) * dimension * dimension);
        printf("Generation 0\n");
    }
    
    MPI_Gather(grid, numberElements, MPI_INT, receivingBuffer, numberElements, MPI_INT, 0, MPI_COMM_WORLD);
    if (p_id == 0) {
        printState(dimension * dimension, dimension, receivingBuffer);
    }

    for (i = 0; i < generations; i++) {
        int j;
        for (j = 0; j < dimension; j++) {
            upperSend[j] = grid[j];
            lowerSend[dimension - j - 1] = grid[numberElements - j - 1];
        }
  
        if (numProcess == 1) {
            rowCpy(lowerSend, upperReceive, dimension);
            rowCpy(upperSend, lowerReceive, dimension);
        } else {
            if (p_id % 2 == 0) {
                MPI_Send(lowerSend, dimension, MPI_INT, next, 2, MPI_COMM_WORLD);
                MPI_Send(upperSend, dimension, MPI_INT, prev, 2, MPI_COMM_WORLD);
                MPI_Recv(upperReceive, dimension, MPI_INT, prev, 2, MPI_COMM_WORLD, &status);
                MPI_Recv(lowerReceive, dimension, MPI_INT, next, 2, MPI_COMM_WORLD, &status);
            } else {
                MPI_Recv(upperReceive, dimension, MPI_INT, prev, 2, MPI_COMM_WORLD, &status);
                MPI_Recv(lowerReceive, dimension, MPI_INT, next, 2, MPI_COMM_WORLD, &status);
                MPI_Send(lowerSend, dimension, MPI_INT, next, 2, MPI_COMM_WORLD);
                MPI_Send(upperSend, dimension, MPI_INT, prev, 2, MPI_COMM_WORLD);
            }
        }
  
        getRow(dimension, 0, grid, currRow);
        if (dimension == numProcess) {
            cellLogic(lowerReceive, currRow, upperReceive, tmpRow, dimension);
            rowCpy(tmpRow, tempGrid, dimension);
        } else {
            getRow(dimension, 1, grid, lowRow);
            cellLogic(lowRow, currRow, upperReceive, tmpRow, dimension);
            rowCpy(tmpRow, tempGrid, dimension);
    
            getRow(dimension, (numberElements/dimension - 1), grid, currRow);
            getRow(dimension, (numberElements/dimension - 2), grid, upRow);
            cellLogic(lowerReceive, currRow, upRow, tmpRow, dimension);
            rowCpy(tmpRow, &tempGrid[numberElements - dimension], dimension);
            
            if ( (numberElements / dimension) > 2) {
                int x;
                for (x = 1; x < (numberElements / dimension) - 1; x++) {
                    getRow(dimension, x + 1, grid, lowRow);
                    getRow(dimension, x, grid, currRow);
                    getRow(dimension, x - 1, grid, upRow);
                    cellLogic(lowRow, currRow, upRow, tmpRow, dimension);
                    rowCpy(tmpRow, &tempGrid[x * dimension], dimension);
                }
            }
        }
        int k;
        for (k = 0; k < numberElements; k++) {
            grid[k] = tempGrid[k];
        }
        
        MPI_Gather(grid, numberElements, MPI_INT, receivingBuffer, numberElements, MPI_INT, 0, MPI_COMM_WORLD);
        
        if (p_id == 0) {
            printf("Generation %d. Updated grid\n", i + 1);
            printState(dimension * dimension, dimension, receivingBuffer); 
        }
    }
    MPI_Finalize;
}

void cellLogic(int * low, int * val, int * up, int * temp, const int dimension) {
    int neighbor[dimension];
    int left = dimension - 1;
    int right = dimension + 1;
    
    int i;
    for (i = 0; i < dimension; i++) {
        neighbor[i] = up[(left + i) % dimension] + up[i] + up[(right + i) % dimension] + 
        low[(left + i) % dimension] + low[i] + low[(right + i) % dimension] +
        val[(left + i) % dimension] + val[(right + i) % dimension];
        
        /*if (neighbor[i] < LOWER_THRESHOLD || neighbor[i] > UPPER_THRESHOLD) {
            temp[i] = DEAD;
        } else if (val[i] != DEAD || neighbor[i] == SPAWN) {
            temp[i] = ALIVE;
        } */
        if (val[i] == 0) {
            if (neighbor[i] == SPAWN) {
                temp[i] = ALIVE;
            } else {
                temp[i] = DEAD;
            }
        } else {
            if (neighbor[i] > UPPER_THRESHOLD || neighbor[i] < LOWER_THRESHOLD) {
                temp[i] = DEAD;
            } else {
                temp[i] = ALIVE;
            }
        }
    }
}

void printState(int size, const int dimension, int * buff) {
    int i;
    for (i = 0; i < size; i++) {
        printf("%d ", buff[i]);
        if ( (i % dimension) == (dimension - 1)) {
            printf("\n");
        }
    }
}
                
void rowCpy(int * source, int * destination, const int dimension) {
    int i;
    for (i = 0; i < dimension; i++) {
        destination[i] = source[i];
    }
}

void getRow(const int dimension, int index, int * grid, int * row) {
    int i;
    for (i = 0; i < dimension; i++) {
        row[i] = grid[index * dimension + i];
    }
}

