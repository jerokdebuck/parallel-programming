import java.util.*;
import java.io.IOException;
        
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.*;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
        
public class FOF {
    // make sure output of mapper is input to reducer 
    public static class Reduce extends Reducer<Text, Text, Text, Text> {
        public void reduce(Text key, Iterable<Text> values, Context context) 
        throws IOException, InterruptedException {
            // get first and second persons
            Text leftKey = getLeft(key);
            Text rightKey = getRight(key);
            // initialize ArrayLists for left and right peoples
            ArrayList<Text> edgesLeft = new ArrayList<Text>();
            ArrayList<Text> edgesRight = new ArrayList<Text>();
            // add to edgesLeft or edgesRight if vals is equal
            // to leftKey or rightKey
            for (Text vals : values) {
                if (getLeft(vals).equals(leftKey)) {
                    edgesLeft.add(getRight(vals));
                } else  {
                    edgesRight.add(getRight(vals));
                }
            }   
            // check for friendship threesomes
            for (Text left : edgesLeft) {
                if (edgesRight.contains(left)) {
                    Text X = leftKey;
                    Text Y = rightKey;
                    Text Z = left;
                    Text w = new Text("<" + X + "," + Y + "," + Z + ">");
                    context.write(left, w);
                }
           }   
        }   
    }   
    public static class Map extends Mapper<Object, Text, Text, Text> {
        public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
            // I used an ArrayList of Text to get the input
            ArrayList<Text> friendsList = new ArrayList<Text>();
            StringTokenizer tok = new StringTokenizer(value.toString());
            // get the usr and iterate over tokenizer to add to friendsList
            Text usr = new Text(tok.nextToken());
            while (tok.hasMoreTokens()) {
                friendsList.add(new Text(tok.nextToken()));
            }
            // iterate over friendsList 
            for (Text friend_one:friendsList) {
                //make and edge between usr and friend_one
                Text outputKey = makeEdge(usr, friend_one);
                for (Text friend_two:friendsList) {
                    // if friend_two is not equal to friend_one, then
                    // we have a friendship
                    if (!friend_one.equals(friend_two)) {
                        Text outputValue = new Text(usr + " " + friend_two);
                        context.write(outputKey, outputValue);
                    }
                }
            }
        }
    } 
    public static Text makeEdge(Text x, Text y) {
	      Text newText;
        // make sure there is an ordering
	      if (x.compareTo(y) < 0) {
	          newText = new Text(x + " " + y);
	      } else {
	          newText = new Text(y + " " + x);
	      }
	   
	      return newText;
	  }

    public static Text getLeft(Text t) {
        // use StringTokenizer to acquire the left person ID
	      StringTokenizer tok = new StringTokenizer(t.toString());
	      return new Text(tok.nextToken());
	  }

    public static Text getRight(Text t) {
        // use StringTokenizer to acquire the right person ID
	      StringTokenizer tok = new StringTokenizer(t.toString());
        // make sure to skip left person first
	      tok.nextToken();
	      return new Text(tok.nextToken());
    }

    public static void main(String[] args) throws Exception {
        // create a new job and configure everything
        Job job = new Job(new Configuration());
        job.setJarByClass(FOF.class);
        job.setJobName("FOF");
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
    
        //Set up map/reduce class
        job.setMapperClass(FOF.Map.class);
        job.setReducerClass(FOF.Reduce.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);
       
        // add input and output 
        FileInputFormat.addInputPath(job, new Path(args[0]));
        FileOutputFormat.setOutputPath(job, new Path(args[1]));        
        job.waitForCompletion(true);
    }

}
