import javax.crypto.*;
import java.security.*;
import javax.crypto.spec.*;

import java.util.Random;

import java.io.PrintStream;

/** Parallel DES key cracker
    @author Darian Hadjiabadi
*/
class ParallelDES implements Runnable
{
  // Cipher for the class
  Cipher des_cipher;
  
  // Key for the class
  SecretKeySpec the_key = null;

  // Byte arrays that hold key block
  byte[] deskeyIN = new byte[8];
  byte[] deskeyOUT = new byte[8];
	
  int id;
  long maxkey;
  SealedObject sldObj;
  long from;
  long to;
  PrintStream p = new PrintStream(System.out);


   
  // Constructor: initialize the cipher
  public ParallelDES(int id, long maxkey, SealedObject sldObj, long from, long to) {
    this.id = id;
    this.maxkey = maxkey;
    this.sldObj = sldObj;
    this.to = to;
    this.from = from;
    try
    {
       des_cipher = Cipher.getInstance("DES");
    }
    catch (Exception e)
    {
      System.out.println("Failed to create cipher.  Exception: " + e.toString());
    }
  }
  public ParallelDES()
  {
    try 
    {
      des_cipher = Cipher.getInstance("DES");
    } 
    catch ( Exception e )
    {
      System.out.println("Failed to create cipher.  Exception: " + e.toString() +
                 " Message: " + e.getMessage()) ; 
    }
  }
  
  // Decrypt the SealedObject
  //
  //   arguments: SealedObject that holds on encrypted String
  //   returns: plaintext String or null if a decryption error
  //     This function will often return null when using an incorrect key.
  //
  public String decrypt ( SealedObject cipherObj )
  {
    try 
    {
      return (String)cipherObj.getObject(the_key);
    }
    catch ( Exception e )
    {
      //      System.out.println("Failed to decrypt message. " + ". Exception: " + e.toString()  + ". Message: " + e.getMessage()) ; 
    }
    return null;
  }
  
  // Encrypt the message
  //
  //  arguments: a String to be encrypted
  //  returns: a SealedObject containing the encrypted string
  //
  public SealedObject encrypt ( String plainstr )
  {
    try 
    {
      des_cipher.init ( Cipher.ENCRYPT_MODE, the_key );
      return new SealedObject( plainstr, des_cipher );
    }
    catch ( Exception e )
    {
      System.out.println("Failed to encrypt message. " + plainstr +
                 ". Exception: " + e.toString() + ". Message: " + e.getMessage()) ; 
    }
    return null;
  }
  
  //  Build a DES formatted key
  //
  //  Convert an array of 7 bytes into an array of 8 bytes.
  //
  private static void makeDESKey(byte[] in, byte[] out)  
  {
    out[0] = (byte) ((in[0] >> 1) & 0xff);
    out[1] = (byte) ((((in[0] & 0x01) << 6) | (((in[1] & 0xff)>>2) & 0xff)) & 0xff);
    out[2] = (byte) ((((in[1] & 0x03) << 5) | (((in[2] & 0xff)>>3) & 0xff)) & 0xff);
    out[3] = (byte) ((((in[2] & 0x07) << 4) | (((in[3] & 0xff)>>4) & 0xff)) & 0xff);
    out[4] = (byte) ((((in[3] & 0x0F) << 3) | (((in[4] & 0xff)>>5) & 0xff)) & 0xff);
    out[5] = (byte) ((((in[4] & 0x1F) << 2) | (((in[5] & 0xff)>>6) & 0xff)) & 0xff);
    out[6] = (byte) ((((in[5] & 0x3F) << 1) | (((in[6] & 0xff)>>7) & 0xff)) & 0xff);
    out[7] = (byte) (   in[6] & 0x7F);
    
    for (int i = 0; i < 8; i++) {
      out[i] = (byte) (out[i] << 1);
    }
  }

  // Set the key (convert from a long integer)
  public void setKey ( long theKey )
  {
    try 
    {
      // convert the integer to the 8 bytes required of keys
      deskeyIN[0] = (byte) (theKey        & 0xFF );
      deskeyIN[1] = (byte)((theKey >>  8) & 0xFF );
      deskeyIN[2] = (byte)((theKey >> 16) & 0xFF );
      deskeyIN[3] = (byte)((theKey >> 24) & 0xFF );
      deskeyIN[4] = (byte)((theKey >> 32) & 0xFF );
      deskeyIN[5] = (byte)((theKey >> 40) & 0xFF );
      deskeyIN[6] = (byte)((theKey >> 48) & 0xFF );

      // theKey should never be larger than 56-bits, so this should always be 0
      deskeyIN[7] = (byte)((theKey >> 56) & 0xFF );
      
      // turn the 56-bits into a proper 64-bit DES key
      makeDESKey(deskeyIN, deskeyOUT);
      
      // Create the specific key for DES
      the_key = new SecretKeySpec ( deskeyOUT, "DES" );
    }
    catch ( Exception e )
    {
      System.out.println("Failed to assign key" +  theKey +
                 ". Exception: " + e.toString() + ". Message: " + e.getMessage()) ;
    }
  }
  //@Override 
  public void run() {
    long runstart = System.currentTimeMillis();
    for ( long i = from; i < to; i++ )
    {
      // Set the key and decipher the object
      this.setKey ( i );
      String decryptstr = this.decrypt ( sldObj );

      // Does the object contain the known plaintext
      if (( decryptstr != null ) && ( decryptstr.indexOf ( "Hopkins" ) != -1 ))
      {
        //  Remote printlns if running for time.
        p.printf("Found decrypt key %016x producing message: %s\n", i , decryptstr);
      }

      // Update progress every once in awhile.
      if ( i % 100000 == 0 )
      {
        long elapsed = System.currentTimeMillis() - runstart;
        System.out.println ( "Thread " + id + " searched key number " + i + " at " + elapsed + " milliseconds.");
      }
    }
    
  }
  
  // Program demonstrating how to create a random key and then search for the key value.
  public static void main ( String[] args )
  {
    if ( 2 != args.length )
    {
      System.out.println ("Usage: java ParallelDES num_threads + key_size_in_bits");
      return;
    }
    

    // Get the arguments
    int num_threads = Integer.parseInt(args[0]);
    long keybits = Long.parseLong ( args[1] );

    long maxkey = ~(0L);
    maxkey = maxkey >>> (64 - keybits);
    
    // Create a simple cipher
    ParallelDES enccipher = new ParallelDES();
    
    // Get a number between 0 and 2^64 - 1
    Random generator = new Random ();
    long key =  generator.nextLong();
    
    // Mask off the high bits so we get a short key
    key = key & maxkey;
    
    // Set up a key
    enccipher.setKey ( key ); 
    
    // Generate a sample string
    String plainstr = "Johns Hopkins afraid of the big bad wolf?";
    
    // Encrypt
    SealedObject sldObj = enccipher.encrypt ( plainstr );
   
    // Here ends the set-up.  Pretending like we know nothing except sldObj,
    // discover what key was used to encrypt the message.
    
    // Get and store the current time -- for timing
    long runstart = System.nanoTime();
    Thread[] keystone = new Thread[num_threads];
    long portion = maxkey / (long) num_threads;
    for (int i = 0; i < num_threads; i++) {
        // this startupcost will divide the data based on the current thread number
        long from = i * portion;
        long to = i < num_threads - 1 ? (i + 1) * portion : maxkey;
	      keystone[i] = new Thread(new ParallelDES(i, maxkey, sldObj, from, to));
    }
    // measures the total start up cost
    long endStartUp = (System.nanoTime() - runstart);
    // launch threads
    for (int j = 0; j < num_threads; j++) {
        keystone[j].start();
    }
    // join threads
    for (int k = 0; k < num_threads; k++) {
        try {
            keystone[k].join();
        }  catch (Exception e) {
	          System.err.println("Could not join " + e.toString() + " Message: " + e.getMessage());
        }
    }
    // output total elapsed time
    long elapsed = System.nanoTime() - runstart;
    long keys = maxkey + 1;
    System.out.println ( "Completed search of " + keys + " keys at " + (elapsed / 1000000) + " milliseconds. Number threads = " + num_threads);
    System.out.println( "Start up costs accounted for: " + endStartUp + " nanoseconds");


  }
}

