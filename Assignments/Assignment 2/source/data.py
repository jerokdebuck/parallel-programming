
# Gets average from data file
#written by Darian Hadjiabadi

def main():
	times = []
	file = open('eight_threads.txt', 'r')
	i = 0
	for line in file:
		if (i % 2 != 0):
			ln = line.split(" ")
			times.append(int(ln[6]))
		i = i + 1
	timeSum = 0
	for i in range(len(times)):
		timeSum += times[i]
	timeSum /= len(times)
	print(timeSum)
		

main()
