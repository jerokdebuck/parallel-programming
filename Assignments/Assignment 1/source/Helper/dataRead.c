/*
  This program reads in an input file in the form of the output
  as put together by filter.c for Parrallel Programming. The data numbers
  are put into column form to be analyzed later.
  Written by Darian Hadjiabadi
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MILLION 1000000

void readFile(FILE *, char[]);

int main(void) {

  // array files contains the file number to read. 
  // user can change to meet his/her needs
  char generic[] = "test";
  char type[] = ".txt";
  int files[] = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
  for (int i = 0; i < sizeof(files) / sizeof(files[0]); i++) {
	  FILE * fptr;
    int num = files[i];
    char fileMinusExtension[strlen(generic) + 30];
    char fileName[strlen(generic) + strlen(type) + 20];
    sprintf(fileMinusExtension, "%s%d", generic, num);
    sprintf(fileName,"%s%d%s", generic, num, type);
	  fptr = fopen(fileName, "r");
    readFile(fptr, fileMinusExtension);
  }
  return 0;
}

void readFile(FILE * fptr, char fileName[]) {

	int placeHolder;
	int dataFirst[32];
	int filterFirst[32];

	char line[512];
	int count = 0;
	int data = 0;
	int filter = 0;

  char first[10];
  char second[10];
  char third[10];
  char fourth[10];
  char fifth[10];
  char sixth[10];

  int num_lines = 0;
	while(fgets(line, 512, fptr) != NULL) {
		if (count % 2 == 0) {
			sscanf(line, "%s %s %s %s %d %s %s %d",first, second, third, fourth, &placeHolder, fifth, sixth, &dataFirst[data]);
			if (placeHolder >= 1) {
				dataFirst[data] += placeHolder * MILLION;
			}
			data += 1;
		} else {
      sscanf(line, "%s %s %s %s %d %s %s %d",first, second, third, fourth, &placeHolder, fifth, sixth, &filterFirst[filter]);
			if (placeHolder >= 1) {
				filterFirst[filter] += placeHolder * MILLION;
			}
			filter += 1;
		}
		count += 1;
    num_lines += 1;
	}
	int filterLength = 1;
  FILE * writer;
  
  char cols[] = "columns";
  char extension[] = ".txt";
  sprintf(fileName,"%s%s%s", fileName, cols, extension);
  writer = fopen(fileName, "w");
	for (int i = 0; i < num_lines / 2; i++) {
		fprintf(writer, "%d %d %d\n", dataFirst[i], filterFirst[i], filterLength);
		filterLength *= 2;
	}
}
			
