import scipy

def openFile(fileNum):
    stringNum = str(fileNum)
    name = 'test' + stringNum + 'columns.txt'
    file = open(name, 'r')
    col1 = []
    col2 = []
    col3 = []
    for line in file:
        columns = line.split(" ")
        col1.append(columns[0])
        col2.append(columns[1])
        col3.append(columns[2])
    file.close()
    return (col1, col2)
    
def writeList(listy, writer):
    for item in listy:
        writer.write("%d\t" % item)
    writer.write("\n")

def main():
    
    filterFirst = []
    dataFirst = []
    for i in range(20):
        one, two =  openFile(i + 1)
        filterFirst.append(two)
        dataFirst.append(one)
    filterSums = []
    dataSums = []
    filterDev = []
    dataDev = []
    for i in range(len(filterFirst[0])):
        filterSum = 0
        dataSum = 0
        filterValues = scipy.zeros((1,20), int)
        dataValues = scipy.zeros((1,20), int)
        for j in range(20):
            filterSum += int(filterFirst[j][i])
            dataSum += int(dataFirst[j][i])
            filterValues[0,j] = filterFirst[j][i]
            dataValues[0,j] = dataFirst[j][i]
        filterSums.append(filterSum)
        dataSums.append(dataSum)
        filterDev.append(scipy.std(filterValues))
        dataDev.append(scipy.std(dataValues))
    filterAvgs = []
    dataAvgs = []
    for i in range(len(filterFirst[0])):
        filterAvgs.append(filterSums[i] / 20)
        dataAvgs.append(dataSums[i] / 20)
  
    writer = open('../statistics.txt', 'w')
    writer.write('Data first averages: ')
    writeList(dataAvgs, writer)
    writer.write('Filter first averages: ')
    writeList(filterAvgs, writer)
    writer.write('Data first deviations: ')
    writeList(dataDev, writer)
    writer.write('Filter first deviations: ')
    writeList(filterDev, writer)
    writer.close()
main()
