#Written by Darian Hadjiabadi for Parallel Programming Spring 2014

#!/usr/bin/env python

import sys

#read lines in file
for line in sys.stdin:
    # split line based off white space
    values = line.split()
    #if list of organized like [a0,a1,...,an], this will acquire
    #[a1,a2,...,an]
    for first_friend in values[1:]:
        #sort everything
        outputKey = sorted([values[0], first_friend])
        #acquire the tail of the sorted list
        for second_friend in values[1:]:
            #if equal, then ignore, else put into tuple
            if first_friend != second_friend:
                outputValue = (first_friend, second_friend)
                print('{0} {1}   {2} {3}'.format(outputKey[0], outputKey[1], outputValue[0], outputValue[1]))
