/*
    Original written by Randall Burns for 600.320 Parallel Programming
    Rewritten by Darian Hadjiabadi for the purposes of assignment 1
*/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <fcntl.h>
#include "omp.h"
#include <math.h>
#include <assert.h>

/* Example filter sizes */
#define DATA_LEN  512*512*128
#define FILTER_LEN  1024


int posix_memalign (void **, size_t, size_t);

/* Subtract the `struct timeval' values X and Y,
    storing the result in RESULT.
    Return 1 if the difference is negative, otherwise 0. */
int timeval_subtract (struct timeval * result, struct timeval * x, struct timeval * y) {
    /* Perform the carry for the later subtraction by updating y. */
    if (x->tv_usec < y->tv_usec) {
        int nsec = (y->tv_usec - x->tv_usec) / 1000000 + 1;
        y->tv_usec -= 1000000 * nsec;
        y->tv_sec += nsec;
    }
    if (x->tv_usec - y->tv_usec > 1000000) {
        int nsec = (x->tv_usec - y->tv_usec) / 1000000;
        y->tv_usec += 1000000 * nsec;
        y->tv_sec -= nsec;
    }
    
    /* Compute the time remaining to wait.
     tv_usec is certainly positive. */
    result->tv_sec = x->tv_sec - y->tv_sec;
    result->tv_usec = x->tv_usec - y->tv_usec;

    /* Return 1 if result is negative. */
    return x->tv_sec < y->tv_sec;
}

/* Function to apply the filter with the filter list in the outside loop */
void serialFilterFirst ( int data_len, unsigned int* input_array, unsigned int* output_array, int filter_len, unsigned int* filter_list ) {
    /* Variables for timing */
    struct timeval ta, tb, tresult;

    /* get initial time */
    gettimeofday ( &ta, NULL );
    /* for all elements in the filter */ 
    for (int y=0; y<filter_len; y++) { 
        /* for all elements in the data */
        for (int x=0; x<data_len; x++) {
            /* it the data element matches the filter */ 
            if (input_array[x] == filter_list[y]) {
                /* include it in the output */
                output_array[x] = input_array[x];
            }
        }
    }
    /* get initial time */
    gettimeofday ( &tb, NULL );

    timeval_subtract ( &tresult, &tb, &ta );

    printf ("Serial filter first took %lu seconds and %lu microseconds.  Filter length = %d\n", tresult.tv_sec, tresult.tv_usec, filter_len );
}


/* Function to apply the filter with the filter list in the outside loop */
void serialDataFirst ( int data_len, unsigned int* input_array, unsigned int* output_array, int filter_len, unsigned int* filter_list, int print ) {
    /* Variables for timing */
    struct timeval ta, tb, tresult;

    /* get initial time */
    gettimeofday ( &ta, NULL );

    /* for all elements in the data */
    for (int x=0; x<data_len; x++) {
        /* for all elements in the filter */ 
        for (int y=0; y<filter_len; y++) { 
            /* it the data element matches the filter */ 
            if (input_array[x] == filter_list[y]) {
            /* include it in the output */
                output_array[x] = input_array[x];
            }
        }
    }

    /* get initial time */
    gettimeofday ( &tb, NULL );

    timeval_subtract ( &tresult, &tb, &ta );
    if (print == 1) {
        printf ("Serial data first took %lu seconds and %lu microseconds.  Filter length = %d\n", tresult.tv_sec, tresult.tv_usec, filter_len );
    }
}

/* Function to apply the filter with the filter list in the outside loop */
void parallelFilterFirst ( int data_len, unsigned int* input_array, unsigned int* output_array, int filter_len, unsigned int* filter_list, int threads ) {
    omp_set_num_threads(threads);
    struct timeval ta, tb, tresult;
    gettimeofday(&ta, NULL);

    #pragma omp parallel for
    for (int y = 0; y < filter_len; y++) {
        for (int x = 0; x < data_len; x++) {
            if (input_array[x] == filter_list[y]) {
                output_array[x] = input_array[x];
            }
        }
    }
    gettimeofday(&tb, NULL);
    timeval_subtract(&tresult, &tb, &ta);
    printf("Parallel filter first took %lu seconds and %lu microseconds. Number threads = %d\n", tresult.tv_sec, tresult.tv_usec, threads);
}


/* Function to apply the filter with the filter list in the outside loop */
void parallelDataFirst ( int data_len, unsigned int* input_array, unsigned int* output_array,int filter_len, unsigned int* filter_list, int threads ) {
    omp_set_num_threads(threads);
    struct timeval ta, tb, tresult;
    gettimeofday(&ta, NULL);
  
    #pragma omp parallel for
    for (int x = 0; x < data_len; x++) {
        for (int y = 0; y < filter_len; y++) {
            if (input_array[x] == filter_list[y]) {
                output_array[x] = input_array[x];
            }
        }
    }
	
    gettimeofday(&tb, NULL);
    timeval_subtract(&tresult, &tb, &ta);
    printf("Parallel data first took %lu seconds and %lu microseconds. Number threads = %d\n", tresult.tv_sec, tresult.tv_usec, threads);
}
int min(int a, int b) {
	if (a < b) {
		return a;
	} 
	return b;
}
void optimized(int data_len, unsigned int * input_array, unsigned int * output_array, int filter_len, unsigned int * filter_list, int threads) {
    omp_set_num_threads(threads);
    struct timeval ta, tb, tresult;
    gettimeofday(&ta, NULL);
	/*
    #pragma omp parallel for
    for (int j = 0; j < filter_len; j++) {
            for (int i = 0; i < data_len; i++) {
	        if(input_array[i] == filter_list[j]) {
                    output_array[i] = filter_list[j];
		}
	    } 
    } */
    unsigned int * filterCurr = filter_list;
    unsigned int * outputCurr = output_array;
    unsigned int * dataCurr = input_array;
    #pragma omp parallel for
    for (int i = 0; i < data_len; i++) {
		unsigned int data = *(dataCurr + i);
	for (int j = 0; j < filter_len; j+=8) {
		if(data == *(filterCurr + j))
		{
			*(outputCurr + i) = data;
		}
		if (data == *(filterCurr + j + 1)) {
			*(outputCurr + i) = data;
		}
	
		if (data == *(filterCurr + j + 2)) {
			*(outputCurr + i) = data;
		}
		if (data == *(filterCurr + j + 3)) {
			*(outputCurr + i) = data;
		}
		if (data == *(filterCurr + j + 4)) {
			
			*(outputCurr + i) = data;
		}
		if (data == *(filterCurr + j + 5)) {
	
			*(outputCurr + i) = data;
		}
		if (data == *(filterCurr + j + 6)) {
			
			*(outputCurr + i) = data;
		}
		if (data == *(filterCurr + j + 7)) {
			*(outputCurr + i) = data;
		} 
		
	}
    }
    gettimeofday(&tb, NULL);
    timeval_subtract(&tresult, &tb, &ta);
    printf("Optimized code took %lu seconds and %lu microseconds. Number of threads = %d, filter length = %d\n", tresult.tv_sec, tresult.tv_usec, threads, filter_len);
}
void checkData ( unsigned int * serialarray, unsigned int * parallelarray ) {
    for (int i=0; i<DATA_LEN; i++) {
        if (serialarray[i] != parallelarray[i]) {
            printf("Data check failed offset %d\n", i );
            return;
        }
    }
}

void serialTesting(unsigned int * input_array, unsigned int * serial_array, unsigned int * output_array, unsigned int * filter_list) {
	
    for (int filter_len = 1; filter_len <= FILTER_LEN; filter_len *= 2) {
        serialDataFirst ( DATA_LEN, input_array, serial_array, filter_len, filter_list, 1 );
        memset ( output_array, 0, DATA_LEN );

        serialFilterFirst ( DATA_LEN, input_array, output_array, filter_len, filter_list );
        checkData ( serial_array, output_array );
        memset ( output_array, 0, DATA_LEN );
    }
}
void parallelSpeedupTesting(unsigned int * input_array, unsigned int * serial_array, unsigned int * output_array, unsigned int * filter_list) {
    // get serial_array all set up 
    serialDataFirst ( DATA_LEN, input_array, serial_array, 512, filter_list, 1);
    memset(output_array, 0, DATA_LEN);
    for (int threads = 1; threads <= 16; threads *= 2) {
        // run on 1, 2, 4, 8, and 16 threads
        parallelDataFirst(DATA_LEN, input_array, output_array, 512, filter_list, threads);
        checkData(serial_array, output_array);
        memset(output_array, 0, DATA_LEN);
        parallelFilterFirst(DATA_LEN, input_array, output_array, 512, filter_list, threads);
        checkData(serial_array, output_array);
        memset(output_array, 0, DATA_LEN);
    }
}

void parallelScaleupTesting(unsigned int * input_array, unsigned int * serial_array, unsigned int * output_array, unsigned int * filter_list) {
    // for each data_len, serial_array and output_array are only found up to position data_len. 
    int threads = 1;
    for (int data_len = 512 * 512 * 8; data_len <= DATA_LEN; data_len *= 2) { // 5 data points
        serialDataFirst(data_len, input_array, serial_array, 512, filter_list, 0);
        memset(output_array, 0, DATA_LEN);
        parallelDataFirst(data_len, input_array, output_array, 512, filter_list, threads);
        checkData(serial_array, output_array);
        memset(output_array, 0, DATA_LEN);
        parallelFilterFirst(data_len, input_array, output_array, 512, filter_list, threads);
        checkData(serial_array, output_array);
        memset(output_array, 0, DATA_LEN);
        // every time data_len doubles, the resources availble (threads) also doubles
        threads *= 2;
    }
}

void optimizedTesting(unsigned int * input_array, unsigned int * serial_array, unsigned int * output_array, unsigned int * filter_list){

    serialDataFirst(DATA_LEN, input_array, serial_array, 512, filter_list,1);
    memset(output_array, 0, DATA_LEN);
    optimized(DATA_LEN, input_array, output_array, 512, filter_list, 8);
    checkData(serial_array, output_array);
    memset(output_array, 0, DATA_LEN);
}
/* main has changed such that it will run a specific test based off the input argument.
   The arguments will be explained in README
*/
int main(int argc, char** argv) {
    /* loop variables */
    int x,y;

    /* Create matrixes */
    unsigned int * input_array;
    unsigned int * serial_array;
    unsigned int * output_array;
    unsigned int * filter_list;

    /* Initialize the data. Values don't matter much. */
    posix_memalign ( (void**)&input_array, 4096,  DATA_LEN * sizeof(unsigned int));
    //input_array = (unsigned int*) posix_memalign ( DATA_LEN * sizeof(unsigned int), 4096);
    for (x=0; x<DATA_LEN; x++) {
        input_array[x] = x % 2048;
    }
    /* start with an empty *all zeros* output array */
    posix_memalign ( (void**)&output_array, 4096, DATA_LEN * sizeof(unsigned int));
    memset ( output_array, 0, DATA_LEN );
    posix_memalign ( (void**)&serial_array, 4096, DATA_LEN * sizeof(unsigned int));
    memset ( serial_array, 0, DATA_LEN );
    /* Initialize the filter. Values don't matter much. */
    filter_list = (unsigned int*) malloc ( FILTER_LEN * sizeof(unsigned int));
    for (y=0; y<FILTER_LEN; y++) {
        filter_list[y] = y;
    }
    if (argc != 2) {
        fprintf(stderr, "ERROR: MUST INPUT ONE (1) COMMAND LINE ARGUMENT. PLEASE READ README FOR MORE INFORMATION\n");
        exit(EXIT_FAILURE);
    }
    int test = atoi(argv[1]);
    if (test == 1) {
        serialTesting(input_array, serial_array, output_array, filter_list);
    } else if (test == 2) {
        parallelSpeedupTesting(input_array, serial_array, output_array, filter_list);
    } else if (test == 3) {
        parallelScaleupTesting(input_array, serial_array, output_array, filter_list);
    } else if (test == 4) {
        optimizedTesting(input_array, serial_array, output_array, filter_list);
    } else {
        fprintf(stderr, "ERROR: ARGUMENT MUST BE INTEGER FROM ONE - FOUR\n");
        exit(EXIT_FAILURE);
    }
    return 0;
}

