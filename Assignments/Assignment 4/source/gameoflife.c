/*
    Darian Hadjiabadi
    Parallel Programming 600.320 
    Assignment 4: gameoflife.c
*/

#include <stdio.h>
#include <stdlib.h>
#include "mpi.h"
#include "life.h" // contains functions

#define DEAD 0
#define ALIVE 1
#define SPAWN 3
#define UPPER_THRESHOLD 3
#define LOWER_THRESHOLD 2

int main(int argc, char ** argv) {

    const int dimension = 16;
    const int global_grid[ 256 ] = {   0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       1,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                       0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
                                  };



    int generations = 64;
    // lets play the game!
    playGame(dimension, global_grid, generations, argc, argv);
    return 0;

}

void init(int * numProcess, int * p_id, int argc, char ** argv) {
    // initialize MPI and get number of processes and current rank
    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, numProcess);
    MPI_Comm_rank(MPI_COMM_WORLD, p_id);
}

void circNeighboring(int numProcess, int p_id, int * next, int * prev) {
    // if only one process, then don't worry about neighbors
    if (numProcess == 1) {
        *next = 0;
        *prev = 0;
    } else {
        // create a circular neighbor construct
        *next = (p_id + 1) % numProcess;
        *prev = (p_id == 0) ? numProcess - 1 : p_id - 1;
    }
}
void playGame(const int dimension, const int * global_grid, int generations, int argc, char ** argv) {
    /* Since I had help trying to get everything started, credit for the first few lines of code here
       go to Matt Gorelik for helping me understand all the pieces that must go together
    */
    int offset, numberElements;
    int lowerSend[dimension], upperSend[dimension];
    int lowerReceive[dimension], upperReceive[dimension];
    
    int lowRow[dimension], upRow[dimension], currRow[dimension], tmpRow[dimension];
    int * grid;
    int *tempGrid;

    int numProcess, p_id;
    int prev, next;
    MPI_Status status;

    init(&numProcess, &p_id, argc, argv);
    circNeighboring(numProcess, p_id, &next, &prev);

    // if the grid cannot be broken down, exit the program
    if (dimension % numProcess != 0) {
        exit(0);
    }
    // each process will take (NxN / Processes) pieces of grid space
    numberElements = (dimension * dimension) / numProcess;
    // and the offset will be based off which process we are currently in
    offset = p_id * numberElements;
    
    // allocate memory based of number of elements
    grid = malloc(sizeof(int) * numberElements);
    tempGrid = malloc(sizeof(int) * numberElements);

    // give the grid variable data from global_grid
    int i;
    for (i = 0; i < numberElements; i++) {
        grid[i] = global_grid[i + offset];
    }

    //Process 0 will output the state of the board, therefore I created a pointer to ints
    // called receivingBuffer which will be used with MPI_Gather to put together the pieces
    // from the other processes
    int * receivingBuffer;
    if (p_id == 0) {
        // if the process is 0, then allocate memory to the receivingBuffer variable
        receivingBuffer = malloc(sizeof(int) * dimension * dimension);
        printf("Pre-Start grid layout\n");
    }
    // gathers values from a group of processes
    MPI_Gather(grid, numberElements, MPI_INT, receivingBuffer, numberElements, MPI_INT, 0, MPI_COMM_WORLD);
    if (p_id == 0) {
        printState(dimension * dimension, dimension, receivingBuffer);
    }
  
    // run through the game
    for (i = 0; i < generations; i++) {
        int j;
        // gets the information to send over via MPI_Send
        for (j = 0; j < dimension; j++) {
            upperSend[j] = grid[j];
            lowerSend[dimension - j - 1] = grid[numberElements - j - 1];
        }
  
        if (numProcess == 1) {
            // copy lowerSend to upperReceive
            rowCpy(lowerSend, upperReceive, dimension);
            // copy upperSend to lowerReceive
            rowCpy(upperSend, lowerReceive, dimension);
        } else {
            // this send-receive pattern is deadlock free
            if (p_id % 2 == 0) {
                // if current processes ID is even, then perform sends to next and prev neighbors
                // to the next neighbor, send the lower part of the border
                // and to the prev neighbor, send the upper part of the border
                MPI_Send(lowerSend, dimension, MPI_INT, next, 2, MPI_COMM_WORLD);
                MPI_Send(upperSend, dimension, MPI_INT, prev, 2, MPI_COMM_WORLD);
                // then perform receives where upperReceive will retrieve information from prev neighbor
                MPI_Recv(upperReceive, dimension, MPI_INT, prev, 2, MPI_COMM_WORLD, &status);
                // and lowerReceive will retrieve information from next neighbor
                MPI_Recv(lowerReceive, dimension, MPI_INT, next, 2, MPI_COMM_WORLD, &status);
            } else {
                // if process ID is odd, perform receives first and then sends. Note that this order/pair of 
                // sends and receives requires that the number of processes is even
                
                MPI_Recv(upperReceive, dimension, MPI_INT, prev, 2, MPI_COMM_WORLD, &status);
                MPI_Recv(lowerReceive, dimension, MPI_INT, next, 2, MPI_COMM_WORLD, &status);
                MPI_Send(lowerSend, dimension, MPI_INT, next, 2, MPI_COMM_WORLD);
                MPI_Send(upperSend, dimension, MPI_INT, prev, 2, MPI_COMM_WORLD);
            }
        }
        // get the first row into currRow
        getRow(dimension, 0, grid, currRow);
        // if the number of processes is equal to the dimension of the grid, then just copy information directory to tmpRow
        // then just copy to tempGrid
        if (dimension == numProcess) {
            cellLogic(lowerReceive, currRow, upperReceive, tmpRow, dimension);
            rowCpy(tmpRow, tempGrid, dimension);
        } else {
            /* Matt Gorelik also helped me a bit with this part, as I was confused how to break the problem down */
            // first row
            getRow(dimension, 1, grid, lowRow);
            cellLogic(lowRow, currRow, upperReceive, tmpRow, dimension);
            rowCpy(tmpRow, tempGrid, dimension);
    
            // last row
            getRow(dimension, (numberElements/dimension - 1), grid, currRow);
            getRow(dimension, (numberElements/dimension - 2), grid, upRow);
            cellLogic(lowerReceive, currRow, upRow, tmpRow, dimension);
            rowCpy(tmpRow, &tempGrid[numberElements - dimension], dimension);
    
            // if more then two processes
            if ( (numberElements / dimension) > 2) {
                int x;
                for (x = 1; x < (numberElements / dimension) - 1; x++) {
                    getRow(dimension, x + 1, grid, lowRow);
                    getRow(dimension, x, grid, currRow);
                    getRow(dimension, x - 1, grid, upRow);
                    cellLogic(lowRow, currRow, upRow, tmpRow, dimension);
                    rowCpy(tmpRow, &tempGrid[x * dimension], dimension);
                }
            }
        }
        // copy information from tempGrid to grid
        int k;
        for (k = 0; k < numberElements; k++) {
            grid[k] = tempGrid[k];
        }
        // gather all information into the receivingBuffer
        MPI_Gather(grid, numberElements, MPI_INT, receivingBuffer, numberElements, MPI_INT, 0, MPI_COMM_WORLD);
        //if process 0, then we have made a circle so we can print the updated state of the game
        if (p_id == 0) {
            printf("Iteration %d. Updated grid\n", i);
            printState(dimension * dimension, dimension, receivingBuffer); 
        }
    }
    MPI_Finalize;
}

void cellLogic(int * low, int * val, int * up, int * temp, const int dimension) {
    int neighbor[dimension];
    int left = dimension - 1;
    int right = dimension + 1;
    
    int i;
    for (i = 0; i < dimension; i++) {
        // Since all cells are either 1 or 0, finding the sum of all surrounding values
        // will determine the number of live cell neighbor a cell has
        neighbor[i] = up[(left + i) % dimension] + up[i] + up[(right + i) % dimension] + 
        low[(left + i) % dimension] + low[i] + low[(right + i) % dimension] +
        val[(left + i) % dimension] + val[(right + i) % dimension];
        // note val[i] was not used because that is the current cell we are looking at
        
        // Game of Life logic 
        if (val[i] == 0) {
            // if dead, a cell must have 3 live neighbors to come alive
            if (neighbor[i] == SPAWN) {
                temp[i] = ALIVE;
            } else {
                temp[i] = DEAD;
            }
        } else {
            // if already alive, a cell must have either 2 or 3 live neighbors to continue living
            if (neighbor[i] > UPPER_THRESHOLD || neighbor[i] < LOWER_THRESHOLD) {
                temp[i] = DEAD;
            } else {
                temp[i] = ALIVE;
            }
        }
    }
}

void printState(int size, const int dimension, int * buff) {
    int i;
    for (i = 0; i < size; i++) {
        printf("%d ", buff[i]);
        if ( (i % dimension) == (dimension - 1)) {
            printf("\n");
        }
    }
}
// copy data from source to destination               
void rowCpy(int * source, int * destination, const int dimension) {
    int i;
    for (i = 0; i < dimension; i++) {
        destination[i] = source[i];
    }
}

// get data
void getRow(const int dimension, int index, int * grid, int * row) {
    int i;
    for (i = 0; i < dimension; i++) {
        row[i] = grid[index * dimension + i];
    }
}

