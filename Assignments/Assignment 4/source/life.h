#ifndef LIFE_H
#define LIFE_H

void printState(int, const int, int*);
void playGame(const int, const int *, int, int, char **);
void rowCpy(int*, int*, const int);
void getRow(const int, int, int*, int*);
void init(int*, int*, int, char**);
void circNeighboring(int, int, int*, int*);
void cellLogic(int *, int *, int *, int *, const int);

#endif //LIFE_H

