#!/usr/bin/env python

import sys
from collections import defaultdict

#destination is empty dictionary
destination = {}
for line in sys.stdin:
    #split line
    alpha, beta, charlie, epsilon = line.split()
    #put into tuples
    key, value = (alpha, beta), (charlie, epsilon)
    #if (alpha,beta) not in the dictionary, then set up a position
    #in dictionary with an empty list
    if key not in destination:
        destination[key] = []
    #append (charlie,epsiolon) to dictionary at key position
    destination[key].append(value)


for k, v in destination.items():
    #if value[0] and key[0] are identical, then we have a friendship
    #so add to left edge variable
    leftEdges = [value[1] for value in v if value[0] == k[0]]
    #if value[0] and key[1] are identical, then we also have a frienship
    #so add to right edge variable
    rightEdges = [value[1] for value in v if value[0] == k[1]]
    
    for left in leftEdges:
        #if also in right edges, then we have a friend threesome
        if left in rightEdges:
            print('{0}: <{1},{2},{3}>'.format(left, k[0], k[1], left))

